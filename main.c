#include <ev.h>
#include "app.h"

int main() {
    struct app app;
    struct ev_loop * loop = ev_default_loop(0);
    app_open(&app, loop);
    ev_loop(loop, 0);
    app_close(&app);
}
