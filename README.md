# i3 focus overlay

Briefly show a red border on top of every window that receives focus.
Useful in combination with `for_window [class="^.*"] border none`.
For some reason not all focus events are sent through the i3 IPC interface,
so it does not always work.

## Installation

    $ make
    ...
    $ sudo make install
    install i3-focus-overlay /usr/bin
