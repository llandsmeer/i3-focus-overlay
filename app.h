#ifndef _APP_H
#define _APP_H

#include <ev.h>
#include <xcb/xcb.h>
#include "i3focus.h"
#include "overlay.h"

struct app {
    int i3fd;
    int xcbfd;
    struct overlay overlay;
    struct ev_io i3watcher;
    struct ev_io xcbwatcher;
    ev_timer timer;
    xcb_connection_t * xcb_connection;
};

void app_open(struct app *, struct ev_loop *);
void app_close(struct app *);

#endif
