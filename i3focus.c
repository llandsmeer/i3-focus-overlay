#define _POSIX_C_SOURCE 2

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>
#include <i3/ipc.h>
#include <yajl/yajl_tree.h>
#include "i3focus.h"

static
int preadline(char * buffer, size_t buffersize, const char * p) {
    int c;
    size_t index = 0;
    FILE * fp = popen(p, "r");
    if (fp == 0) {
        return -1;
    }
    while (index + 1 < buffersize) {
        c = getc(fp);
        if (c == EOF || c == '\n') break;
        buffer[index] = c;
        index += 1;
    }
    buffer[index] = 0;
    pclose(fp);
    return (int)index;
}

static
uint32_t u32read(int fd) {
    char type_buf[4];
    read(fd, type_buf, sizeof(type_buf));
    return
        ((uint64_t)type_buf[0] << 0) |
        ((uint64_t)type_buf[1] << 8) |
        ((uint64_t)type_buf[2] << 16) |
        ((uint64_t)type_buf[3] << 24);
}

static
int u32write(int fd, uint32_t n) {
    char size_buf[4] = {
        (n >> 0) & 0xff, (n >> 8) & 0xff,
        (n >> 16) & 0xff, (n >> 24) & 0xff,
    };
    return write(fd, size_buf, sizeof(size_buf));
}

static
int i3socket() {
    int fd, pathlength;
    struct sockaddr_un addr;
    char path[1024];
    pathlength = preadline(path, sizeof(path), "i3 --get-socketpath");
    if (pathlength == -1) {
        return -1;
    }
    fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd == -1) {
        return -1;
    }
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, path, pathlength);
    connect(fd, (struct sockaddr*)&addr, sizeof(addr));
    return fd;
}

static
int i3write(int fd, int type, const char * msg, size_t size) {
    write(fd, I3_IPC_MAGIC, strlen(I3_IPC_MAGIC));
    u32write(fd, (uint32_t)size);
    u32write(fd, (uint32_t)type);
    return write(fd, msg, size);
}

static
int i3write_s(int fd, int type, const char * msg) {
    return i3write(fd, type, msg, strlen(msg));
}

struct i3msg {
    int type;
    char * msg;
};

static
int i3readmsg(int fd, struct i3msg * i3msg) {
    char proto_buf[strlen(I3_IPC_MAGIC)];
    read(fd, proto_buf, sizeof(proto_buf));
    size_t length = u32read(fd);
    i3msg->type = u32read(fd);
    char * buffer = malloc(length + 1);
    read(fd, buffer, length);
    buffer[length] = 0;
    i3msg->msg = buffer;
    return 0;
}

static
char * i3reply(int fd) {
    struct i3msg i3msg;
    int err =  i3readmsg(fd, &i3msg);
    if (err == 0) {
        return i3msg.msg;
    } else {
        return 0;
    }
}

static
char * i3communicate(int fd, int type, const char * msg) {
    i3write_s(fd, type, msg);
    return i3reply(fd);
}

static
void i3expect(int fd, int type, const char * msg, const char * expected) {
    char * reply = i3communicate(fd, type, msg);
    if (strcmp(reply, expected) != 0) {
        fprintf(stderr, "%s\n", msg);
        fprintf(stderr, "%s\n", reply);
        exit(EXIT_FAILURE);
    }
    free(reply);
}

static
int parse_rect(yajl_val node, struct i3window * i3window) {
    static const char * path_rect[] = {"rect", 0},
                      * path_x[] = {"x", 0},
                      * path_y[] = {"y", 0},
                      * path_width[] = {"width", 0},
                      * path_height[] = {"height", 0};
    yajl_val rect_node = yajl_tree_get(node, path_rect, yajl_t_object);
    if (!rect_node) {
        return -1;
    }
    i3window->x =
        YAJL_GET_INTEGER(yajl_tree_get(rect_node, path_x, yajl_t_number));
    i3window->y =
        YAJL_GET_INTEGER(yajl_tree_get(rect_node, path_y, yajl_t_number));
    i3window->width =
        YAJL_GET_INTEGER(yajl_tree_get(rect_node, path_width, yajl_t_number));
    i3window->height =
        YAJL_GET_INTEGER(yajl_tree_get(rect_node, path_height, yajl_t_number));
    return 0;
}

static
int parse_tree(yajl_val node, struct i3window * i3window) {
    static const char * path_focused[] = {"focused", 0},
                      * path_nodes[] = {"nodes", 0};
    if (!node) return -1;
    yajl_val node_focused = yajl_tree_get(node, path_focused, yajl_t_any);
    int focused = YAJL_IS_TRUE(node_focused);
    if (focused) {
        return parse_rect(node, i3window);
    }
    yajl_val nodes = yajl_tree_get(node, path_nodes, yajl_t_array);
    for (size_t i = 0; i < nodes->u.array.len; i++) {
        yajl_val child = nodes->u.array.values[i];
        if (parse_tree(child, i3window) == 0) {
            return 0;
        }
    }
    return -1;
}

int i3focus_open() {
    int fd = i3socket();
    if (fd == -1) {
        fprintf(stderr, "failed to open i3 ipc socket\n");
        exit(EXIT_FAILURE);
    }
    i3expect(fd, I3_IPC_MESSAGE_TYPE_SUBSCRIBE, "[\"window\"]", "{\"success\":true}");
    i3focus_invalidate(fd);
    return fd;
}

void i3focus_invalidate(int fd) {
    i3write_s(fd, I3_IPC_MESSAGE_TYPE_GET_TREE, "");
}

int i3focus_read(int fd, struct i3window * i3window) {
    static const char * path_change[] = {"change", 0},
                      * path_container[] = {"container", 0};
    struct i3msg i3msg;
    int err, retval = -1;
    err = i3readmsg(fd, &i3msg);
    if (err != 0) {
        return err;
    }
    yajl_val node = yajl_tree_parse(i3msg.msg, 0, 0);
    if (!node) {
        free(i3msg.msg);
        return -1;
    }
    if (i3msg.type == I3_IPC_EVENT_WINDOW) {
        const char * change = YAJL_GET_STRING(yajl_tree_get(node, path_change, yajl_t_string));
        yajl_val container = yajl_tree_get(node, path_container, yajl_t_object);
        if (strcmp(change, "focus") == 0) {
            retval = parse_rect(container, i3window);
        } else if (strcmp(change, "new") == 0) {
            i3focus_invalidate(fd);
        } else if (strcmp(change, "close") == 0) {
            i3focus_invalidate(fd);
        } else {
            puts(change);
        }
    } else if (i3msg.type == I3_IPC_REPLY_TYPE_TREE) {
        retval = parse_tree(node, i3window);
    } else {
        printf("%d\n", i3msg.type);
    }
    yajl_tree_free(node);
    free(i3msg.msg);
    return retval;
}
