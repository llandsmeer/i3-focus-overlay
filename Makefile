CC=cc
CFLAGS=-std=c99 -pedantic -Wall -Werror -flto
LDFLAGS=-lxcb -lyajl -lev

SRC=app.c i3focus.c main.c overlay.c
OBJ=$(SRC:.c=.o)

i3-focus-overlay: $(OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o i3-focus-overlay $(OBJ)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

.PHONY: clean
clean:
	rm -f $(OBJ) i3-focus-overlay

.PHONY: install
install: i3-focus-overlay
	install i3-focus-overlay /usr/bin
