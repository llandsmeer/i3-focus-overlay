#include <stdlib.h>
#include "app.h"

static
void hide(EV_P_ struct ev_timer * w, int revents) {
    struct app * app = (struct app*)w->data;
    overlay_hide(&app->overlay);
}

static
void focus(struct ev_loop * loop, struct ev_io * w, int revents) {
    struct app * app = (struct app*)w->data;
    struct i3window e;
    if (i3focus_read(app->i3fd, &e) != -1) {
        overlay_show(&app->overlay, e.x, e.y, e.width, e.height, 8);
        ev_timer_stop(loop, &app->timer);
        ev_timer_set(&app->timer, 0.5, 0);
        ev_timer_start(loop, &app->timer);
    }
}

static
void keyboard(struct ev_loop * loop, struct ev_io * w, int revents) {
    struct app * app = (struct app*)w->data;
    xcb_generic_event_t * event = xcb_poll_for_event(app->xcb_connection);
    if (!event) {
        return;
    }
    overlay_write(&app->overlay, event);
    free(event);
}

void app_open(struct app * app, struct ev_loop * loop) {
    xcb_connection_t * connection = xcb_connect(0, 0);
    app->i3fd = i3focus_open();
    app->xcbfd = xcb_get_file_descriptor(connection);
    overlay_open(&app->overlay, connection);
    ev_init(&app->i3watcher, &focus);
    ev_init(&app->xcbwatcher, &keyboard);
    app->i3watcher.data = app;
    app->xcbwatcher.data = app;
    app->timer.data = app;
    ev_io_set(&app->i3watcher, app->i3fd, EV_READ);
    ev_io_set(&app->xcbwatcher, app->xcbfd, EV_READ);
    ev_io_start(loop, &app->i3watcher);
    ev_io_start(loop, &app->xcbwatcher);
    ev_timer_init(&app->timer, &hide, 1, 0);
}

void app_close(struct app * app) {
    xcb_disconnect(app->xcb_connection);
}
