#include "overlay.h"

#include <stdio.h> // DEBUG

static
xcb_window_t create_window(xcb_connection_t * connection, xcb_screen_t * screen) {
    xcb_window_t window = xcb_generate_id(connection);
    uint32_t mask = XCB_CW_BACK_PIXEL | XCB_CW_OVERRIDE_REDIRECT;
    uint32_t values[2] = { 0xffff0000, 1 };
    xcb_create_window(connection,
            XCB_COPY_FROM_PARENT,
            window,
            screen->root,
            0, 0,
            1, 1,
            0,
            XCB_WINDOW_CLASS_INPUT_OUTPUT,
            screen->root_visual,
            mask, values);
    return window;
}

void overlay_open(struct overlay * overlay, xcb_connection_t * connection) {
    overlay->connection = connection;
    const xcb_setup_t * setup = xcb_get_setup(connection);
    xcb_screen_iterator_t iter = xcb_setup_roots_iterator(setup);
    xcb_screen_t * screen = iter.data;
    overlay->screen = screen;
    overlay->top = create_window(connection, screen);
    overlay->left = create_window(connection, screen);
    overlay->right = create_window(connection, screen);
    overlay->bottom = create_window(connection, screen);
    xcb_flush(connection);
}

void overlay_show(struct overlay * overlay, int x, int y, int w, int h, int border) {
    xcb_connection_t * connection = overlay->connection;
    const uint32_t XYWH = XCB_CONFIG_WINDOW_X
                        | XCB_CONFIG_WINDOW_Y
                        | XCB_CONFIG_WINDOW_WIDTH
                        | XCB_CONFIG_WINDOW_HEIGHT;
    xcb_map_window(overlay->connection, overlay->top);
    xcb_map_window(overlay->connection, overlay->left);
    xcb_map_window(overlay->connection, overlay->right);
    xcb_map_window(overlay->connection, overlay->bottom);
    uint32_t xywh[4];
    xywh[0] = x; xywh[1] = y; xywh[2] = w; xywh[3] = border;
    xcb_configure_window(connection, overlay->top, XYWH, xywh);
    xywh[0] = x; xywh[1] = y; xywh[2] = border; xywh[3] = h;
    xcb_configure_window(connection, overlay->left, XYWH, xywh);
    xywh[0] = x+w-border; xywh[1] = y; xywh[2] = border; xywh[3] = h;
    xcb_configure_window(connection, overlay->right, XYWH, xywh);
    xywh[0] = x; xywh[1] = y+h-border; xywh[2] = w; xywh[3] = border;
    xcb_configure_window(connection, overlay->bottom, XYWH, xywh);
    xcb_flush(overlay->connection);
}

void overlay_hide(struct overlay * overlay) {
    xcb_unmap_window(overlay->connection, overlay->top);
    xcb_unmap_window(overlay->connection, overlay->left);
    xcb_unmap_window(overlay->connection, overlay->right);
    xcb_unmap_window(overlay->connection, overlay->bottom);
    xcb_flush(overlay->connection);
}

void overlay_write(struct overlay * overlay, xcb_generic_event_t * event) {
    puts("event");
}
