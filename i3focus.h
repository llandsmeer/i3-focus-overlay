#ifndef _I3_FOCUS
#define _I3_FOCUS

struct i3window {
    int x;
    int y;
    int width;
    int height;
};

int i3focus_open();
int i3focus_read(int, struct i3window *);
void i3focus_invalidate(int);

#endif
