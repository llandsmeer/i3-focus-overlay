#ifndef _OVERLAY_H
#define _OVERLAY_H

#include <xcb/xcb.h>

struct overlay {
    xcb_connection_t * connection;
    xcb_screen_t * screen;
    xcb_window_t top;
    xcb_window_t left;
    xcb_window_t right;
    xcb_window_t bottom;
};

void overlay_open(struct overlay *, xcb_connection_t * connection);
void overlay_show(struct overlay *, int x, int y, int w, int h, int border);
void overlay_hide(struct overlay *);
void overlay_write(struct overlay *, xcb_generic_event_t *);

#endif
